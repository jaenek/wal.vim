hi clear
set background=dark

if exists('syntax_on')
    syntax reset
endif

" Colorscheme name
let g:colors_name = 'wal'

" General
hi Normal ctermbg=NONE ctermfg=15
hi NonText ctermbg=NONE ctermfg=7
hi Underlined ctermbg=NONE ctermfg=8 cterm=underline
hi StatusLine ctermbg=0 ctermfg=15 cterm=bold,reverse
hi StatusLineNC ctermbg=8 ctermfg=0
hi TabLine ctermbg=NONE ctermfg=8
hi TabLineFill ctermbg=NONE ctermfg=8
hi TabLineSel ctermbg=4 ctermfg=0
hi TermCursorNC ctermbg=3 ctermfg=0
hi VertSplit ctermbg=8 ctermfg=0
hi Title ctermbg=NONE ctermfg=4
hi CursorLine ctermbg=NONE ctermfg=NONE cterm=NONE
hi CursorLineNr ctermbg=NONE ctermfg=2 cterm=bold
hi LineNr ctermbg=NONE ctermfg=8
hi helpLeadBlank ctermbg=NONE ctermfg=7
hi helpNormal ctermbg=NONE ctermfg=7
hi Visual ctermbg=0 ctermfg=15 cterm=reverse term=reverse
hi VisualNOS ctermbg=NONE ctermfg=1
hi FoldColumn ctermbg=NONE ctermfg=8
hi Folded ctermbg=NONE ctermfg=8
hi IncSearch ctermbg=3 ctermfg=0
hi Search ctermbg=3 ctermfg=0
hi Directory ctermbg=NONE ctermfg=4
hi MatchParen ctermbg=8 ctermfg=0
hi ColorColumn ctermbg=NONE ctermfg=7 cterm=reverse
hi signColumn ctermbg=NONE ctermfg=4
hi ErrorMsg ctermbg=1 ctermfg=0
hi ModeMsg ctermbg=NONE ctermfg=2
hi MoreMsg ctermbg=NONE ctermfg=2
hi Question ctermbg=NONE ctermfg=4
hi WarningMsg ctermbg=NONE ctermfg=1
hi Cursor ctermbg=2 ctermfg=0 cterm=reverse
hi CursorColumn ctermbg=8 ctermfg=7
hi SpellBad ctermbg=NONE ctermfg=1 cterm=underline
hi SpellCap ctermbg=NONE ctermfg=4 cterm=underline
hi SpellLocal ctermbg=NONE ctermfg=5 cterm=underline
hi SpellRare ctermbg=NONE ctermfg=6 cterm=underline
" Code
hi Boolean ctermbg=NONE ctermfg=5
hi Float ctermbg=NONE ctermfg=6
hi Number ctermbg=NONE ctermfg=6
hi String ctermbg=NONE ctermfg=2
hi Constant ctermbg=NONE ctermfg=1
hi Character ctermbg=NONE ctermfg=1
hi Structure ctermbg=NONE ctermfg=4
hi Conditional ctermbg=NONE ctermfg=1
hi Define ctermbg=NONE ctermfg=5
hi Delimiter ctermbg=NONE ctermfg=5
hi Include ctermbg=NONE ctermfg=1
hi Keyword ctermbg=NONE ctermfg=5
hi Label ctermbg=NONE ctermfg=3
hi Operator ctermbg=NONE ctermfg=4
hi Repeat ctermbg=NONE ctermfg=1
hi SpecialChar ctermbg=NONE ctermfg=1
hi SpecialKey ctermbg=NONE ctermfg=15
hi Tag ctermbg=NONE ctermfg=4
hi Typedef ctermbg=NONE ctermfg=3
hi Type ctermbg=NONE ctermfg=4
hi Identifier ctermbg=NONE ctermfg=1 cterm=BOLD
hi PreProc ctermbg=NONE ctermfg=3
hi Special ctermbg=NONE ctermfg=1
hi Statement ctermbg=NONE ctermfg=1
hi Comment ctermbg=NONE ctermfg=8
hi Error ctermbg=NONE ctermfg=1 cterm=reverse
hi Ignore ctermbg=8 ctermfg=0
hi Todo ctermbg=NONE ctermfg=6 cterm=BOLD
" C
hi cOperator ctermbg=NONE ctermfg=4
hi cPreCondit ctermbg=NONE ctermfg=5
" Go
hi goSpaceError ctermbg=0 ctermfg=NONE
hi goType ctermbg=NONE ctermfg=4
hi goExtraType ctermbg=NONE ctermfg=4
hi goSignedInts ctermbg=NONE ctermfg=4
hi goBuiltins ctermbg=NONE ctermfg=4
hi goDeclaration ctermbg=NONE ctermfg=5
hi goLabel ctermbg=NONE ctermfg=5
hi goString ctermbg=NONE ctermfg=2
hi goDecimalInt ctermbg=NONE ctermfg=6
hi goFloat ctermbg=NONE ctermfg=6
" Python
hi pythonOperator ctermbg=NONE ctermfg=3
hi pythonFunction ctermbg=NONE ctermfg=4
hi pythonRepeat ctermbg=NONE ctermfg=3
hi pythonStatement ctermbg=NONE ctermfg=5 cterm=Bold
hi pythonBuiltIn ctermbg=NONE ctermfg=4
" VimScript
hi vimUserCommand ctermbg=NONE ctermfg=1 cterm=BOLD
	hi link vimMap vimUserCommand
	hi link vimLet vimUserCommand
	hi link vimCommand vimUserCommand
	hi link vimFTCmd vimUserCommand
	hi link vimAutoCmd vimUserCommand
	hi link vimNotFunc vimUserCommand
hi vimNotation ctermbg=NONE ctermfg=4
hi vimMapModKey ctermbg=NONE ctermfg=4
hi vimBracket ctermbg=NONE ctermfg=7
hi vimCommentString ctermbg=NONE ctermfg=8
" Autocomplete menu
hi WildMenu ctermbg=2 ctermfg=0
hi Pmenu ctermbg=7 ctermfg=0
hi PmenuSel ctermbg=2 ctermfg=0 cterm=bold
hi PmenuSbar ctermbg=7 ctermfg=7
hi PmenuThumb ctermbg=8 ctermfg=8
" Diff
hi DiffAdd ctermbg=NONE ctermfg=2
hi DiffChange ctermbg=NONE ctermfg=8
hi DiffDelete ctermbg=NONE ctermfg=1
hi DiffText ctermbg=NONE ctermfg=4
hi diffAdded ctermbg=NONE ctermfg=2
hi diffRemoved ctermbg=NONE ctermfg=1
hi diffLine ctermbg=NONE ctermfg=5
" Xresources
hi xdefaultsValue ctermbg=NONE ctermfg=7
" HTML
hi htmlLink ctermbg=NONE ctermfg=6 cterm=underline
hi htmlBold ctermbg=NONE ctermfg=15 cterm=BOLD
hi htmlItalic ctermbg=NONE ctermfg=15
hi htmlEndTag ctermbg=NONE ctermfg=4
hi htmlTag ctermbg=NONE ctermfg=4
hi htmlTagName ctermbg=NONE ctermfg=4 cterm=BOLD
hi htmlH1 ctermbg=NONE ctermfg=4
	hi link htmlH2 htmlH1
	hi link htmlH3 htmlH1
	hi link htmlH4 htmlH1
	hi link htmlH5 htmlH1
	hi link htmlH6 htmlH1
" CSS
hi cssMultiColumnAttr ctermbg=NONE ctermfg=2
	hi link cssFontAttr cssMultiColumnAttr
	hi link cssFlexibleBoxAttr cssMultiColumnAttr
hi cssBraces ctermbg=NONE ctermfg=2
	hi link cssAttrComma cssBraces
hi cssValueLength ctermbg=NONE ctermfg=7
hi cssUnitDecorators ctermbg=NONE ctermfg=7
hi cssValueNumber ctermbg=NONE ctermfg=7
	hi link cssValueLength cssValueNumber
hi cssNoise ctermbg=NONE ctermfg=8
hi cssTagName ctermbg=NONE ctermfg=1
hi cssFunctionName ctermbg=NONE ctermfg=4
" Markdown
hi markdownH1 ctermbg=NONE ctermfg=7
	hi link markdownH2 markdownH1
	hi link markdownH3 markdownH1
	hi link markdownH4 markdownH1
	hi link markdownH5 markdownH1
	hi link markdownH6 markdownH1
hi markdownAutomaticLink ctermbg=NONE ctermfg=2 cterm=underline
	hi link markdownUrl markdownAutomaticLink
hi markdownError ctermbg=NONE ctermfg=7
hi markdownCode ctermbg=NONE ctermfg=3
hi markdownCodeBlock ctermbg=NONE ctermfg=3
hi markdownCodeDelimiter ctermbg=NONE ctermfg=5
hi markdownItalic cterm=Italic
hi markdownBold cterm=Bold
" NERDTree
hi NERDTreeDirSlash ctermbg=NONE ctermfg=4
hi NERDTreeExecFile ctermbg=NONE ctermfg=2
